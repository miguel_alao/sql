CREATE TABLE <tablename> (
	<column name> <data type> 
	<constrains... e.g. NOT NULL, PRIMARY KEY, 
	UNIQUE, CHECK()> [REFERENCES 
	<other_table>(other_table_column)],
	...
	CHECK(condition),
	PRIMARY KEY(column 1, column 2, ...),
	FOREIGN KEY(column 1, column 2, ...), 
		REFERENCES other_table(
		other_table_column1,
		other_table_column2, ...)
)...;

DROP TABLE <tablename>;
DELETE FROM <tablename> [WHERE <condition>
];

UPDATE <tablename>
	SET column 1 = new.value1, column 2 =
		new_value2, ...
	[WHERE <condition>];

INSERT INFO <tablename>
	[ ( column 1, column 2, ...) ]
VALUES
	( value1_1, value1_2, ...),
	( value2_1, value2_2, ...);

SELECT
	<expression> [AS <alias>],
	....
FROM 
	<table>[, <table2>, ...]
[GROUP BY column1, column2, ... ASC | DESC]
[WHERE <conditions>];



