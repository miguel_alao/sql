const express = require("express");
const bodyParser = require("body-parser");
const sqlite = require("sqlite");

let g_db = null;

const server = express();
server.set("view engine", "pug");

server.use(bodyParser.urlencoded({
	extend: true
}));

server.get("/", async (req, res) => {
  const posts = await g_db.get("SELECT COUNT(*) AS num_topic_today FROM topic WHERE date_open = DATE('now')");
  const num_topic_today = posts.num_topic_today;

  const displayed_posts = await g_db.all("SELECT * FROM post ORDER BY date_posted DESC");

  res.render("main", {
    num_topic_today,
    displayed_posts
  });
});

server.post("/posts", async (req, res) => {

  await g_db.run("INSERT INTO topic ( feedback, date_open ) VALUES ( $feedback, DATE('now') )", {
    $feedback: req.body.feedback
  });

  res.redirect("/");
});

sqlite.open("./forum.sqlite").then(db => {
  g_db = db;
  server.listen(8080);
  console.log("running");
});
