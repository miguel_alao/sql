CREATE TABLE topic (
	id integer primary key,
	title text not null,
	date_open text not null default datetime ('now'),
);

CREATE TABLE post (
	id integer primary key,
	topic_id integer not null references topic,
	date_posted text not default datetime ('now'),
	msg text not null
);


