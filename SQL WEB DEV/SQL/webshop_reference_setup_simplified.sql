PRAGMA foreign_keys = On;

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  email TEXT NOT NULL UNIQUE,
  passwordHash CHAR(50) NOT NULL,
  default_address_id INTEGER REFERENCES addresses
);

CREATE TABLE addresses (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL REFERENCES users,
  city TEXT NOT NULL,
  street TEXT NOT NULL,
  houseno INTEGER NOT NULL
);

--ALTER TABLE users ADD FOREIGN KEY(default_address_id) default_address_id_fk REFERENCES addresses(id);

CREATE TABLE sellers (
  user_id INTEGER NOT NULL PRIMARY KEY REFERENCES users
);

CREATE TABLE products (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT NOT NULL,

  -- NOTE: you'd normally want each seller to be able to have a different price for the same product; 
  --we don't do this here for simplicity's sake
  price REAL NOT NULL CHECK(price > 0)
);

CREATE TABLE sold_products (
  seller_id INTEGER NOT NULL REFERENCES sellers,
  product_id INTEGER NOT NULL REFERENCES products,
  PRIMARY KEY(seller_id, product_id)
);

CREATE TABLE orders (
  id INTEGER PRIMARY KEY,
  buyer_id INTEGER NOT NULL REFERENCES users,
  shipping_address_id INTEGER NOT NULL REFERENCES addresses
  --billing_address_id INTEGER NOT NULL REFERENCES addresses
);

CREATE TABLE orders_products (
  order_id INTEGER NOT NULL REFERENCES orders,
  product_id INTEGER NOT NULL REFERENCES products,
  seller_id INTEGER NOT NULL REFERENCES sellers,
  quantity INTEGER NOT NULL DEFAULT 1 CHECK(quantity > 0),
  FOREIGN KEY(product_id, seller_id) REFERENCES sold_products(product_id, seller_id),
  PRIMARY KEY(order_id, product_id, seller_id)
);
