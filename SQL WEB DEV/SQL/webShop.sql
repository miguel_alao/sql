pragma foreign_keys = on;

begin;

drop table users;

create table users (
	id integer primary key,
	name text not null,
	email text unique not null,
	password_hash CHAR(50) not null,
	primary_address_id integer addresses
);

insert into users
	(name, email, password_hash)
values
	('Mr Potato', 'potato@potato.nl', 'potato'),
	('John Doe', 'john.doe@gmail.com', 'johndoe99'),
	('Foo Bar', 'foo@bar.com', 'stuff'),
	('Joe the Reaper', 'joe@death.com', '666666');

drop table addresses;

create table addresses (
	id integer primary key,
	user_id integer not null references users,
	city text not null,
	street text not null,
	house_number integer not null
);

insert into addresses
	(user_id, city, street, house_number)
values
	((select from users Where name = 'Mr Potato'), 'Potatotown', 'Potato Avenue', 42),
	((select from users Where name = 'Joe the Reaper'), 'Lalaland', 'Lalala street', 10),
	((select from users Where name = 'John Doe'), 'Berlin', 'Foobarstrasse', 99),
	((select from users Where name = 'Foo Bar'), 'Amsterdam', 'Schipluidenlaan', 666);

drop table sellers

create table sellers (
	user_id integer not null primary key references users
);

insert into sellers
	(user_id)
values
	((select from users where name = 'Mr Potato') ),
	((select from users where name = 'Joe the Reaper') );

drop table products;

create table products (
	id integer not null primary key,
	user_id integer primary key references sellers,
	name text not null,
	details text not null,
	price real not null check(price > 0)
);

insert into products
	(name, details, price)
values
	( 'Bag of Potatos', '1 kg of high quality handsome potatoes', 10 ),
	( 'Death', 'The sweet release of death. Only 5 EURs for gift wrapping!', 50 );

drop table orders;

create table orders (
	id integer not null primary key,
	user_id integer not null references users,
	address_id integer not null references addresses
);

insert into orders
	(user_id)

drop table ordered_items;

create table ordered_items (
	seller_id integer not null references sellers,
	product_id integer not null references products,
	amount_items integer not null default 1,
	primary key(product_id, order_id)
);


INSERT INTO ordered_items
	( seller_id, product_id)
VALUES
	(
	(SELECT id FROM users WHERE name = 'Mr Potato'),
    (SELECT id FROM products WHERE name = 'Bag of Potatos')
	),
	(
    (SELECT id FROM users WHERE name = 'Joe the Reaper'),
    (SELECT id FROM products WHERE name = 'Bag of Potatos')
	),
	(
    (SELECT id FROM users WHERE name = 'Joe the Reaper'),
    (SELECT id FROM products WHERE name = 'Death')
	);