CREATE TABLE ... <tablename> (
  <column name> <data type> <extra constraints... e.g. NOT NULL, PRIMARY KEY, UNIQUE, CHECK()> 
  [REFERENCES <other_table>(other_table_column)],
  ...
  CHECK(condition),
  PRIMARY KEY(column1, column2, ...),
  FOREIGN KEY(column1, column2, ...) REFERENCES other_table(other_table_column1, other_table_column2, ...)
) ...;

DROP TABLE <tablename>;
DELETE FROM <tablename> [WHERE <condition>];

UPDATE <tablename>
  SET column1 = new_value1, column2 = new_value2, ...
  [WHERE <condition>];

INSERT INTO <tablename>
  [ ( column1, column2, ... ) ]
VALUES
  ( value1_1, value1_2, ... ),
  ( value2_1, value2_2, ... )
  ...;

SELECT
  <expression> [AS <alias>],
  ...
FROM
  <table1>[, <table2>, ...]
[INNER|LEFT|RIGHT|FULL JOIN <other_table_name> ON <join_condition>]
...
[GROUP BY column1, column2, ...]
[ORDER BY column1, column2, ... ASC | DESC]
[WHERE <conditions>];

--"rowid"