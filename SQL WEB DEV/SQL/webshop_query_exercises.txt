For each exercise, note down the SQL query/queries you used to achieve the result.


These exercises should all be solved with a single query. 
None of the exercises require the use of sub-queries.


1) List all products that cost more than 600.


2) Same as 1) except in ascending order of price.


3) Show the most expensive product.


4) List the number of addresses belonging to each user. 
    You don't have include users who do not have any addresses in the results and 
    it's enough to only show the user_id and the number of addresses in the results.


5) Show all addresses and the name of the user they belong to.


6) Show the primary address (not the ID, the address text) of each user who has one.


7) Same as
 6) except show NULL (appears as an empty cell) for address for the users who do not 
    have a primary address.


8) Show the number of registered users.


9) List the number of addresses belonging to each individual user. 
    Show the ID and name of the user, as well as the their number of addresses 
    in the results. If a user doesn't have any addresses, he should still be included 
    in the results!


10) Show which sellers don't have a registered address. 
   Show both the seller ID and the name of the seller.


1 select price > 40 from poducts;
2 select price from products order by price desc;
3 select max(price) from products;
select * from products where price = (select max(price) from products);
4 select houseno from addresses;
5 select * from addresses inner join users;

1 select * from products where price > 40;
2 select * from products where price > 40 order by price asc;
3 select * from products order by price desc limit 1;
   select * from products where price = (select max(price) from products);
4 select user_id, count(*) as num_addresses from addresses group by user_id;
5 select addresses, *, users.name from addresses inner join users on addresses.user_id = users.id;
6 select 
          users.name, 
          addresses.street || ''|| addresses.houseno ||','|| addresses city as default_addresses
                  from users 
                       inner join addresses 
                                 on addresses.id = users.default_addresses_id;
7 select 
          users.name, 
          addresses.street || ''|| addresses.houseno ||','|| addresses city as default_addresses
                  from users 
                       left join addresses 
                                 on addresses.id = users.default_addresses_id;
8 select count(*) as num_users from users;
9 select  users.id, users.name count (addresses.id) as num_addresses
           from users
                  left join addresses
                                 on users.id = addresses.user_id
                                         group by addresses.user_id;
10 select sellers.user_id, users.name
            from selleres
                inner join users
                            on sellers.user_id = users.id
                                                   left join addresses
                                                                   on addresses.user_id = users.id
                                                                               where addresses.user_id is null;

select users,*, addresses, * from users inner join addresses on users.id = addresses.user.id
select users,*, addresses, * from users inner join addresses on


select * from users where default_addresses_id is null; 

SELECT column_name(s)

FROM table1
INNER JOIN table2 ON table1.column_name = table2.column_name;

.width 30

