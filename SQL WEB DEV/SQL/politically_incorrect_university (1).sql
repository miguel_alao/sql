pragma foreign_keys = off;

begin;

drop table users;

create table users (
  id integer primary key,
  email text unique not null,
  passwordHash text not null
);

insert into users
  ( email, passwordHash )
values
  ( 'potato@potato.nl', 'potato potato' ),
  ( 'kom@kommer.nl', 'komkommer' ),
  ( 'jesus@carpenter.nl', 'fuck judas 666' );

drop table students;

create table students (
  user_id integer primary key references users,
  name text not null
);

insert into students
  ( user_id, name )
values
  ( (select id from users where email = 'potato@potato.nl'), 'Mr Potato Potato' ),
  ( (select id from users where email = 'kom@kommer.nl'), 'Ms Komkommer' );

drop table teachers;

create table teachers (
  user_id integer primary key references users,
  name text not null
);

insert into teachers
  ( user_id, name )
values
  ( (select id from users where email = 'jesus@carpenter.nl'), 'Mr Jesus the One and Only' ),
  ( (select id from users where email = 'kom@kommer.nl'), 'Mr Komkommer' );

drop table courses;

create table courses (
  id integer primary key,
  name text not null,
  department text,
  level integer not null check(level > 0)
);

insert into courses
  ( name, department, level )
values
  ( 'Satanism', null, 666 ),
  ( 'BDSM', 'Sex Education', 69 ),
  ( 'Sexual cannibalism', 'Sex Education', 100 );

drop table teachers_courses;

create table teachers_courses (
  teacher_id integer not null references teachers,
  course_id integer not null references courses,
  primary key(teacher_id, course_id)
);

insert into teachers_courses
  ( teacher_id, course_id )
values
  (
    (select user_id from teachers where name = 'Mr Jesus the One and Only'),
    (select id from courses where name = 'Satanism')
  ),
  (
    (select user_id from teachers where name = 'Mr Jesus the One and Only'),
    (select id from courses where name = 'BDSM')
  ),
  (
    (select user_id from teachers where name = 'Mr Komkommer'),
    (select id from courses where name = 'BDSM')
  );

drop table grades;

create table grades (
  student_id integer not null references students,
  course_id integer not null references courses,
  grade tinyint(2) not null check(grade > 0 and grade <= 10),
  primary key(student_id, course_id)
);

commit;
