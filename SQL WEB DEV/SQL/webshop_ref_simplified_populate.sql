PRAGMA foreign_keys = On;

BEGIN;

INSERT INTO users
  ( name, email, passwordHash )
VALUES
  ( 'Mr Potato', 'potato@potato.nl', 'potato' ),
  ( 'John Doe', 'john.doe@gmail.com', 'johndoe99' ),
  ( 'Foo Bar', 'foo@bar.com', 'stuff' ),
  ( 'Joe the Reaper', 'joe@death.com', '666666666' );

INSERT INTO sellers
  ( user_id )
VALUES
  ( (SELECT id FROM users WHERE name = 'Mr Potato') ),
  ( (SELECT id FROM users WHERE name = 'Joe the Reaper') );

INSERT INTO addresses
  ( user_id, city, street, houseno )
VALUES
  ( (SELECT id FROM users WHERE name = 'Mr Potato'), 'Potatotown', 'Potato Avenue', 42 ),
  ( (SELECT id FROM users WHERE name = 'Mr Potato'), 'Lalaland', 'Lalala street', 10 ),
  ( (SELECT id FROM users WHERE name = 'John Doe'), 'Berlin', 'Foobarstrasse', 99 ),
  ( (SELECT id FROM users WHERE name = 'Foo Bar'), 'Amsterdam', 'Schipluidenlaan', 666 );

UPDATE users
SET default_address_id = (
  SELECT id
  FROM addresses
  WHERE city = 'Potatotown'
  LIMIT 1
)
WHERE name = 'Mr Potato';

UPDATE users
SET default_address_id = (
  SELECT id
  FROM addresses
  WHERE city = 'Berlin'
  LIMIT 1
)
WHERE name = 'John Doe';

UPDATE users
SET default_address_id = (
  SELECT id
  FROM addresses
  WHERE city = 'Amsterdam'
  LIMIT 1
)
WHERE name = 'Foo Bar';

INSERT INTO products
  ( name, description, price )
VALUES
  ( 'Bag of Potatos', '1 kg of high quality handsome potatoes', 10 ),
  ( 'Death', 'The sweet release of death. Only 5 EURs for gift wrapping!', 50 );

INSERT INTO sold_products
  ( seller_id, product_id )
VALUES
  (
    (SELECT id FROM users WHERE name = 'Mr Potato'),
    (SELECT id FROM products WHERE name = 'Bag of Potatos')
  ),
  (
    (SELECT id FROM users WHERE name = 'Joe the Reaper'),
    (SELECT id FROM products WHERE name = 'Bag of Potatos')
  ),
  (
    (SELECT id FROM users WHERE name = 'Joe the Reaper'),
    (SELECT id FROM products WHERE name = 'Death')
  );

COMMIT;
